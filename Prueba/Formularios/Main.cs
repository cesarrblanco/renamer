﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Prueba
{
    public partial class Form1 : Form
    {
        string[] files;
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                ObtenerFicheros(fbd.SelectedPath);
            }

        }

        private void ObtenerFicheros(string path)
        {
            listView1.Items.Clear();
            textBox1.Text = path;
            files = Directory.GetFiles(path, "*.*");

            for (int i = 0; i < files.Length; i++)
            {
                files[i] = Path.GetFileName(files[i]);
                ListViewItem lvi = new ListViewItem(files[i]);
                listView1.Items.Add(lvi);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            foreach (var file in files)
            {                
                ListViewItem lvi = new ListViewItem(file);
                string cambio = file;
                string textoRemplazable = GetTextoRemplazable(file);
                if (textoRemplazable != "")
                { cambio = file.Replace(textoRemplazable, txCambio.Text); }                
                lvi.SubItems.Add(cambio);
                listView1.Items.Add(lvi);
            }
        }

        private string GetTextoRemplazable(string Original)
        {
            string textoRemplazable="";
            if (string.IsNullOrEmpty(txFinal.Text))
            {
                textoRemplazable = txInicial.Text;
            }
            else
            {
                int start = Original.IndexOf(txInicial.Text);
                if (start > 0)
                {
                    int end = Original.IndexOf(txFinal.Text, start);
                    if (end > 0)
                    {
                        end = end + txFinal.Text.Length;
                        textoRemplazable = Original.Substring(start, end - start); }
                }
            }

            return textoRemplazable;
        }
    

        private void button4_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            foreach (var file in files)
            {
                string Original=file;
                string cambio = file;
                string textoRemplazable = GetTextoRemplazable(Original);
                if (textoRemplazable != "")
                { cambio = file.Replace(textoRemplazable,txCambio.Text); }
                ListViewItem lvi = new ListViewItem(Original);
                lvi.SubItems.Add(cambio);
                listView1.Items.Add(lvi);
                File.Move(textBox1.Text + "\\" + Original, textBox1.Text + "\\" + cambio);               
            }
            ObtenerFicheros(textBox1.Text);
        }

        private void btReferescar_Click(object sender, EventArgs e)
        {
            ObtenerFicheros(textBox1.Text);
        }
    }
}
